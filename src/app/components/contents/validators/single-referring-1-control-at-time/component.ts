import {
    Component,
    OnInit,
    ViewChild
} from '@angular/core';
import {
    FormGroup,
    FormBuilder,
    Validators as AngularValidators,
    FormControl
} from '@angular/forms';

import { Validators } from '@cyberjs.on/ng-web-compack/validators';


@Component({
    selector: 'app-form-validation-as-single-over-1-control-at-time',
    templateUrl: './template.html',
    styleUrls: ['./style.less']
})
export class SingleReferring1ControlAtTimeExampleComponent implements OnInit {

    @ViewChild('single41ControlAtTimeValidator', { static: false })
    private single41ControlAtTimeValidator!: Validators.SingleReferring1ControlAtTimeComponent;

    form: FormGroup;

    constructor(
        private formBuilder: FormBuilder
    ) {

        this.form = this.formBuilder.group({
            name: new FormControl('',
                {
                    validators: [
                        AngularValidators.required,
                        AngularValidators.minLength(4)
                    ],
                    updateOn: 'blur'
                }
            )
            , name2: new FormControl('',
                {
                    validators: [AngularValidators.required],
                    updateOn: 'blur'
                }
            )
        });

    }

    ngOnInit() {

    }

    onSubmit() {
        this.single41ControlAtTimeValidator.validate(this.form);
    }

}
