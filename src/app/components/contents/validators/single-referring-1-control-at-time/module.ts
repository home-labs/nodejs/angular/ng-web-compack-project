import { NgModule } from '@angular/core';

import { SingleReferring1ControlAtTimeModule } from '@cyberjs.on/ng-web-compack/validators';

import { SingleReferring1ControlAtTimeExampleComponent } from './component';


@NgModule({
    imports: [
        SingleReferring1ControlAtTimeModule.forChild()
    ],
    declarations: [
        SingleReferring1ControlAtTimeExampleComponent
    ],
    exports: [
        SingleReferring1ControlAtTimeModule,
        SingleReferring1ControlAtTimeExampleComponent
    ],
    providers: []
})
export class SingleReferring1ControlAtTimeExampleModule { }
