import { NgModule } from '@angular/core';
import {
    Routes,
    RouterModule
} from '@angular/router';

import {
    Over1ControlAtTimeExampleComponent,
    OverControlExampleComponent,
    OverControlAsTemplateDrivenExampleComponent,
    SingleListExampleComponent,
    SingleReferring1ControlAtTimeExampleComponent
} from './index';


const routes: Routes = [
    {
        path: 'over-1-control-at-time',
        component: Over1ControlAtTimeExampleComponent
    },
    {
        path: 'over-control',
        children: [
            {
                path: 'reactive-form',
                component: OverControlExampleComponent
            },
            {
                path: 'template-driven',
                component: OverControlAsTemplateDrivenExampleComponent
            }
        ]
    },
    {
        path: 'single-list',
        component: SingleListExampleComponent
    },
    {
        path: 'single-referring-1-control-at-time',
        component: SingleReferring1ControlAtTimeExampleComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class RoutingModule { }
