import { NgModule } from '@angular/core';

import { ListModule } from '@cyberjs.on/ng-web-compack/validators';

import { SingleListExampleComponent } from './component';


@NgModule({
    imports: [
        ListModule.forChild()
    ]
    , declarations: [
        SingleListExampleComponent
    ]
    , exports: [
        SingleListExampleComponent
    ],
    providers: [

    ]
})
export class SingleListExampleModule { }
