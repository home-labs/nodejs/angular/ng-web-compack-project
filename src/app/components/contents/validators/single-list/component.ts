import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators as AngularValidators,
  FormControl
} from '@angular/forms';

import { Validators } from '@cyberjs.on/ng-web-compack/validators';


@Component({
  selector: 'app-form-validation-as-single-list',
  templateUrl: './template.html',
  styleUrls: ['./style.less']
})
export class SingleListExampleComponent implements OnInit {

  @ViewChild('validatorAsList', { static: false })
  private validatorAsList!: Validators.SingleListComponent;

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) {

    this.form = this.formBuilder.group({
      name: new FormControl('',
        {
          validators: [
            AngularValidators.required,
            AngularValidators.minLength(4)
          ],
          updateOn: 'blur'
        }
      )
      , name2: new FormControl('',
        {
          validators: [
            AngularValidators.required,
            AngularValidators.minLength(4)
          ],
          updateOn: 'blur'
        }
      )
    });

  }

  ngOnInit() {

  }

  onSubmit() {
    this.validatorAsList.validate(this.form);
  }

}
