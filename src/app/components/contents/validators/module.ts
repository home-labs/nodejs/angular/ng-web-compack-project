import { NgModule } from '@angular/core';

import { RoutingModule } from './routes';

import {
    Over1ControlAtTimeExampleModule,
    OverControlExampleModule,
    OverControlAsTemplateDrivenExampleModule,
    SingleListExampleModule,
    SingleReferring1ControlAtTimeExampleModule
} from './index';


@NgModule({
    imports: [
        RoutingModule,
        Over1ControlAtTimeExampleModule,
        OverControlExampleModule,
        OverControlAsTemplateDrivenExampleModule,
        SingleListExampleModule,
        SingleReferring1ControlAtTimeExampleModule
    ],
    declarations: [],
    exports: [],
    providers: []
})
export class ValidatorsExampleModule { }
