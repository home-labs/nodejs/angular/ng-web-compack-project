import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators as AngularValidators,
  FormControl
} from '@angular/forms';

import { Validators } from '@cyberjs.on/ng-web-compack/validators';


@Component({
  selector: 'app-form-validation-as-over-1-control-at-time',
  templateUrl: './template.html',
  styleUrls: ['./style.less']
})
export class Over1ControlAtTimeExampleComponent implements OnInit {

  form: FormGroup;

  constructor(
    private validator: Validators.Notifier,
    private formBuilder: FormBuilder
  ) {

    this.form = this.formBuilder.group({
      name: new FormControl('',
        {
          validators: [
            AngularValidators.required,
            AngularValidators.minLength(4)
          ],
          updateOn: 'blur'
        }
      )
      , name2: new FormControl('',
        {
          validators: [
            AngularValidators.required,
            AngularValidators.minLength(4)
          ],
          updateOn: 'blur'
        }
      )
    });

  }

  ngOnInit() {

  }

  onSubmit() {
    this.validator.notify();
  }

}
