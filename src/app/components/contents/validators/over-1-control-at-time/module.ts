import { NgModule } from '@angular/core';

import { Over1ControlAtTimeModule } from '@cyberjs.on/ng-web-compack/validators';

import { Over1ControlAtTimeExampleComponent } from './component';


@NgModule({
  imports: [
    Over1ControlAtTimeModule.forChild()
  ],
  declarations: [
    Over1ControlAtTimeExampleComponent
  ],
  exports: [
    Over1ControlAtTimeModule,
    Over1ControlAtTimeExampleComponent
  ],
  providers: [

  ]
})
export class Over1ControlAtTimeExampleModule { }
