import {
  Component,
  OnInit
} from '@angular/core';

import { Validators } from '@cyberjs.on/ng-web-compack/validators';


import { ModelTemplateExample } from './model-template-example';


@Component({
  selector: 'app-form-validation-as-over-control',
  templateUrl: './template.html',
  styleUrls: ['./style.less']
})
export class OverControlAsTemplateDrivenExampleComponent implements OnInit {

  modelReference: ModelTemplateExample;

  constructor(
    private validator: Validators.Notifier
  ) {
    this.modelReference = {
      name: undefined,
      name2: undefined
    };
  }

  ngOnInit() {

  }

  onSubmit() {
    this.validator.notify();
  }

}
