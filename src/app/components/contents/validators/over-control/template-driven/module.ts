import { NgModule } from '@angular/core';

import { OverControlModule } from '@cyberjs.on/ng-web-compack/validators';

import { OverControlAsTemplateDrivenExampleComponent } from './component';


@NgModule({
  imports: [
    OverControlModule.forChild()
  ],
  declarations: [
    OverControlAsTemplateDrivenExampleComponent
  ],
  exports: [
    OverControlAsTemplateDrivenExampleComponent
  ],
  providers: []
})
export class OverControlAsTemplateDrivenExampleModule { }
