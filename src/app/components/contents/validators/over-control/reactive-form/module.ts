import { NgModule } from '@angular/core';

import { OverControlModule } from '@cyberjs.on/ng-web-compack/validators';

import { OverControlExampleComponent } from './component';


@NgModule({
    imports: [
        OverControlModule.forChild()
    ],
    declarations: [
        OverControlExampleComponent
    ],
    exports: [
        OverControlModule,
        OverControlExampleComponent
    ],
    providers: []
})
export class OverControlExampleModule { }
