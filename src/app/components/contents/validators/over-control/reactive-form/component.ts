import {
    Component,
    OnInit
} from '@angular/core';
import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators as AngularValidators
} from '@angular/forms';

import { Validators } from '@cyberjs.on/ng-web-compack/validators';


@Component({
    selector: 'app-form-validation-as-over-control',
    templateUrl: './template.html',
    styleUrls: ['./style.less']
})
export class OverControlExampleComponent implements OnInit {

    form: FormGroup;

    constructor(
        private validator: Validators.Notifier,
        private formBuilder: FormBuilder
    ) {

        this.form = this.formBuilder.group({
            name: new FormControl('',
                {
                    validators: [
                        AngularValidators.required,
                        AngularValidators.minLength(4)
                    ],
                    updateOn: 'blur'
                }
            )
            , name2: new FormControl('',
                {
                    validators: [AngularValidators.required],
                    updateOn: 'blur'
                }
            )
        });

    }

    ngOnInit() {

    }

    onSubmit() {
        this.validator.notify();
    }

}
