import { NgModule } from '@angular/core';

import { MatcheckModule } from '@cyberjs.on/ng-web-compack/matcheck';

import { MatcheckExampleComponent } from './component';


@NgModule({
  imports: [
    MatcheckModule
  ],
  declarations: [
    MatcheckExampleComponent
  ],
  exports: [
    MatcheckModule
    , MatcheckExampleComponent
  ],
  providers: []
})
export class MatcheckExampleModule { }
