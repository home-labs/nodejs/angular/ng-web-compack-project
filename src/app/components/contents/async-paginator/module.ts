import { NgModule } from '@angular/core';

import { AsyncPaginatorComponent } from './component';

import { PageBrowserModule } from '@cyberjs.on/ng-web-compack/page-browser';


@NgModule({
    imports: [
        PageBrowserModule
    ],
    declarations: [
        AsyncPaginatorComponent
    ],
    exports: [
        AsyncPaginatorComponent
    ],
    providers: []
})
export class AsyncPaginatorModule { }
