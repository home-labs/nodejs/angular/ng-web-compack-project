import {
    AfterViewInit,
    Component,
    ViewChild
} from '@angular/core';

import { PowerRange } from '@cyberjs.on/power-range';

import { PageBrowserComponent } from '@cyberjs.on/ng-web-compack/page-browser';


type TestUser = {

    property1: string;

    property2?: string;

}


@Component({
    selector: 'app-async-paginator',
    templateUrl: './template.html',
    styleUrls: ['./style.less']
})
export class AsyncPaginatorComponent implements AfterViewInit {

    @ViewChild('pageBrowser')
    private pageBrowser!: PageBrowserComponent;

    collectionPromise!: Promise<TestUser[]>;

    enablePageNumberInputBox: boolean;

    limit: number;

    queryParamPropertyName: string;

    widthGrowthToggleFactor: number;

    // usado com o pipe para obter inline uma página de uma coleção
    // pageNumber!: number;

    ready2Show!: Promise<boolean>;

    private collection!: TestUser[];

    private pagination!: PowerRange.Pagination<TestUser>;

    constructor() {
        this.limit = 5;

        this.enablePageNumberInputBox = true;

        this.queryParamPropertyName = `pagina`;

        this.widthGrowthToggleFactor = 7.5;

        this.initialize();
    }

    ngAfterViewInit() {

        this.ready2Show = new Promise(
            async (accomplish: (collection: boolean) => void) => {

                await this.calculateTotalPages();

                if (!this.collectionPromise) {
                    await this.setPage(1);
                }

                accomplish(false);

            }
        );

    }

    async onChangePage(pageNumber: number) {
        // this.pageNumber = pageNumber;

        await this.ready2Show;

        this.setPage(pageNumber);
    }

    private async setPage(pageNumber: number): Promise<void> {

        this.collectionPromise = new Promise(
            (accomplish: (collection: TestUser[]) => void) => {

                const interval = setTimeout(
                    () => {

                        accomplish(this.pagination.getPage(pageNumber));

                        clearInterval(interval);
                    }, 700
                );

            }
        );

    }

    private calculateTotalPages(): Promise<void> {

        return new Promise(

            (accomplish: () => void) => {

                const interval = setTimeout(
                    () => {
                        this.pageBrowser.totalPages = PowerRange.Pagination
                            .calculateTotalPages(this.collection.length, this.limit);

                        accomplish();

                        clearInterval(interval);

                    }, 1600
                );

            }
        );

    }

    private initialize() {

        this.collection = [];

        for (let i = 1; i <= 10000; i++) {
            this.collection.push({
                property1: `property1 value ${i}`
                , property2: `property2 value ${i}`
            });
        }

        this.pagination = new PowerRange.Pagination(this.collection, this.limit);
    }

}
