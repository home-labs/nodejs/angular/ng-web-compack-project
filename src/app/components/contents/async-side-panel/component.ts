import {
  Component,
  ViewChild
} from '@angular/core';

import { SidePanel } from '@cyberjs.on/ng-web-compack/side-panel';


@Component({
  selector: 'app-async-side-panel',
  templateUrl: './template.html',
  styleUrls: ['./style.less']
})
export class AsyncSidePanelExampleComponent {

  @ViewChild('asyncSidePanel')
  asyncSidePanel!: SidePanel.SidePanelComponent;

  promise: Promise<any>;

  content: any;

  private _accomplish: Function;

  constructor(

  ) {
    this._accomplish = function () { };

    this.promise = new Promise(
      (accomplish: (value: any[]) => void) => {
        this._accomplish = accomplish;
      }
    );

    setTimeout(
      () => {
        this.content = `Async example works!`;

        this._accomplish(this.content);
      }, 1700
    );
  }

  onClick() {
    this.asyncSidePanel.toggle();
  }

}
