import { NgModule } from '@angular/core';

import { AsyncSidePanelExampleComponent } from './component';

import { SidePanelModule } from '@cyberjs.on/ng-web-compack/side-panel';


@NgModule({
  imports: [
    SidePanelModule
  ],
  declarations: [
    AsyncSidePanelExampleComponent
  ],
  exports: [
    AsyncSidePanelExampleComponent
  ],
  providers: []
})
export class AsyncSidePanelExampleModule { }
