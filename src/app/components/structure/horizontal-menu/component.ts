import {
    Component,
    OnInit
} from '@angular/core';
import {
    RouterLink,
    RouterLinkActive
} from '@angular/router';


@Component({
    selector: 'app-horizontal-menu',
    standalone: true,
    imports: [
        RouterLink,
        RouterLinkActive
    ],
    templateUrl: './template.html',
    styleUrls: ['./style.less']
})
export class HorizontalMenuComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
