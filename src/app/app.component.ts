import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    RouterLink,
    RouterLinkActive,
    RouterOutlet
} from '@angular/router';
import { FormsModule } from '@angular/forms';

import { HorizontalMenuComponent } from './components/structure/horizontal-menu/component';
import {
    AsyncPaginatorModule,
    AsyncSidePanelExampleModule,
    MatcheckExampleModule
} from './components/contents';


@Component({
    selector: 'app-root',
    standalone: true,
    imports: [
        CommonModule,
        RouterOutlet,
        FormsModule,
        HorizontalMenuComponent,
        AsyncPaginatorModule,
        AsyncSidePanelExampleModule,
        MatcheckExampleModule
    ],
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent {

    constructor(

    ) { }

}
