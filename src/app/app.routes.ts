import { Routes } from '@angular/router';

import {
    AsyncPaginatorComponent,
    AsyncSidePanelExampleComponent,
    MatcheckExampleComponent
} from './components/contents/index';


export const routes: Routes = [
    // objetos literais vazios ({}) geram erro
    {
        path: '',
        // trabalhando como uma home page
        redirectTo: 'validators/over-control/reactive-form',
        pathMatch: 'full'
    },
    {
        path: 'async-paginator',
        component: AsyncPaginatorComponent
    },
    {
        path: 'async-side-panel',
        component: AsyncSidePanelExampleComponent
    },
    {
        path: 'matcheck',
        component: MatcheckExampleComponent
    },
    {
        path: 'validators',
        // lazy loading?
        loadChildren: () => import('./components/contents/validators/module')
            .then(m => m.ValidatorsExampleModule)
    }
];
