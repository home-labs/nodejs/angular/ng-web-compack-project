# Matcheck

## Dependencies

- @actjs.on/ng-cool-action-fit-pipe 1.0.0-1.

## Installing

	$ npm i @cyberjs.on/ng-web-compack --save

## API reference

### Usage

Include the module into `imports` metadata key of `NgModule` decorator in your application, importing `MatcheckModule` from `@cyberjs.on/ng-web-compack/matcheck`, like that.

```typescript
import { MatcheckModule } from '@cyberjs.on/ng-web-compack/matcheck';

@NgModule({
    imports: [
        MatcheckModule
    ]
})
export class MyModule() { }
```

### Directives

Selector: `lib-mat-checkbox-group`
Exported as:  `MatCheckboxGroupComponent`

#### Properties

|Name|Description|
|--|--|
|@Input()<br/>parent: [ThisType\<T>](https://www.typescriptlang.org/docs/handbook/utility-types.html#thistypet)||
|@Input()<br/>form: [FormGroup](https://angular.io/api/forms/FormGroup) \| [NgForm](https://angular.io/api/forms/NgForm)||
|@Input()<br/>ngClass: any|[Angular NgClass Direcrive](https://angular.io/api/common/NgClass)||
|@Input()<br/>objectCollection: object[]|
|@Input()<br/>property: string|Property name of an object of the collection defined in `objectCollection`. That is optional, but if you don't define it you'll have to define `formProperty`.|
|@Input()<br/>formProperty: string|Optional. If it's not define, it'll receive the `property` value.|
|@Input()<br/>filterTerm: string|Optional. A term of string type to filter objects defined in `objectCollection`, by `propertyLabel`.|
|@Input()<br/>propertyLabel: string||
|@Input()<br/>callbackDeclarationOfDisabledProperty: ((matCheckbox: [MatCheckbox](https://material.angular.io/components/checkbox/api#MatCheckbox)) => boolean)|Optional.|

<!--stackedit_data:
eyJoaXN0b3J5IjpbMjgwMTg1MjkxLDg0NDcxODUzMCwyMDkwMT
MyMTFdfQ==
-->