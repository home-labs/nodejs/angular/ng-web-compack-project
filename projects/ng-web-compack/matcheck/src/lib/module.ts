import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    FormsModule,
    ReactiveFormsModule
} from '@angular/forms';
import {
    MatCheckboxModule
} from '@angular/material/checkbox';

import { NgCoolactionFitPipeModule } from '@cyberjs.on/ng-coolaction-fit-pipe';

import { MatCheckboxGroupComponent } from './component/matCheckboxGroup.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatCheckboxModule,
        NgCoolactionFitPipeModule
    ],
    declarations: [
        MatCheckboxGroupComponent
    ],
    exports: [
        CommonModule,
        MatCheckboxGroupComponent,
        MatCheckboxModule,
        FormsModule,
        ReactiveFormsModule
    ]
})
export class MatcheckModule { }
