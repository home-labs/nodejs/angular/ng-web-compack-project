/**
 * Public API Surface of ng-matcheck
 *
 * ~(tilde) means only patch update: 1.0.0 to 1.0.x;
 * ^(caret) means only minor update: 1.0.0 to 1.x.x.
 */

// ~(tilde) 1.0.0 to 1.0.x; ^(caret) means 1.0.0 to 1.x.x. So ~ does only patch update, ^, minor

export * from './src/index';
