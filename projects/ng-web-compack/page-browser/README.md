# PageBrowser

## Dependencies

- @cyberjs.on/basicss 1.0.0-0;
- @cyberjs.on/power-range 1.0.0-1.

## Installing

	$ npm i @cyberjs.on/ng-web-compack --save

## Usage

Include the module into `imports` metadata key of `NgModule` decorator in your application, importing `PageBrowserModule` from `@cyberjs.on/ng-web-compack/page-browser`, like that.

```ts
import { PageBrowserModule } from '@cyberjs.on/ng-web-compack/page-browser';

@NgModule({
    imports: [
        PageBrowserModule 
    ]
})
export class MyModule() { }
```

## Data and Event Binding

```html
<lib-page-browser
  #pageBrowser
  [labelTranslations]="{
    firstPage: 'first',
    previousPage: '«',
    nextPage: '»',
    lastPage: 'last'
  }"
  [enablePageNumberInputBox]="enablePageNumberInputBox"
  [queryParamPropertyName]="'page'"
  (changePage)="onChangePage($event)"
></lib-page-browser>
```

So...

```ts
import {
    Component
    , OnInit,
    , ViewChild
} from '@angular/core';

import {
    PageBrowserComponent,
    PowerRange
} from '@cyberjs.on/ng-web-compack/page-browser';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.styl']
})
export class AppComponent implements OnInit {

    @ViewChild('pageBrowser', { static: true }) private pageBrowser: PageBrowserComponent;

    enablePageNumberInputBox: boolean;

    collectionPromise!: Promise<TestUser[]>;

    limit: number;

    pageNumber: number;

    private count: number;

    constructor() {
        this.limit = 5;
        this.enablePageNumberInputBox = true;

        this.pageNumber = 1;

        this.count = 1;
    }

    async ngOnInit() {

        await this.getPage();

        this.pageBrowser.totalPages = PowerRange.Pagination
            .calculatesTotalPages(this.count, this.limit);
    }

    // protected for now it doesn't work when building the lib
    onChangePage(pageNumber: number) {
        this.pageNumber = pageNumber;
    }

    private getPage(): Promise<TestUser[]> {

        const collection: TestUser[] = [];

        this.collectionPromise = new Promise(
            (accomplish: (collection: TestUser[]) => void) => {

                const interval = setTimeout(
                    () => {
                        this.count = 10000;

                        for (let i = 1; i <= this.count; i++) {
                            collection.push({
                                property1: `property1 value ${i}`
                                , property2: `property2 value ${i}`
                            });
                        }

                        accomplish(collection);

                        console.log('function called after an interval');
                        clearInterval(interval);
                    }, 1500
                );

            }
        );

        return this.collectionPromise;
    }

}
```

### Directives

Selector: `lib-page-browser`
Exported as:  `PageBrowserComponent`

#### Properties


Name|Description
--|--
|@Input()<br/>labelTranslations: ILabelTranslationsProperties|Optional. A literal object to define control labels.|
|@Input()<br/>enablePageNumberInputBox: boolean|Optional. A text box to insert the page number to jump to any page in the page range.|
|@Input()<br/>queryParamPropertyName: string|Optional. Define the name of query param property.|
|@Input()<br/>widthGrowthToggleFactor: number|Optional. A number that represents the growth factor of the page number input text box.|
|@Output<br/>changePage: EventEmitter<number>|Optional. Callback function to inform what's the number of current page.|


**Note.**: 
>- don't use this component nested a HTML tag block with `*ngIf` directive if you is using an `Angular` last than 8 version, or it'll not work;
>- `enablePageNumberInputBox` is optional, if you set it, you'll can browse to a specific page, just clicking on the page number box to enable the page number input box (text input type) and clicking in previous page button (if the page number is smaller than current) or next page button (if the page number is bigger than current). If you enable the page number input box you can set `widthGrowthToggleFactor` to define the width growth factor of the page number input box;
>- You can use the `getPagePer` pipe passing the page number and the limit to page a collection `(... | getPagePer:pageNumberVariable:limitVariable)` inline.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE3Njg5NjcyMzEsLTQ1MDc5MjU3Myw1MD
c0ODE1OTJdfQ==
-->