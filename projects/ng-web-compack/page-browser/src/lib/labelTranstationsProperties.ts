export type LabelTranslationsProperties = {

    firstPage?: string | HTMLElement;

    previousPage?: string | HTMLElement;

    nextPage?: string | HTMLElement;

    lastPage?: string | HTMLElement;

}
