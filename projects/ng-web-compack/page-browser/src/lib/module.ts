import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { PageBrowserComponent } from './component/pageBrowser.component';

import { GetPagePerPipe } from './pipes/index';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild([])
    ],
    declarations: [
        GetPagePerPipe,
        PageBrowserComponent
    ],
    exports: [
        CommonModule,
        PageBrowserComponent,
        GetPagePerPipe
    ]
})
export class PageBrowserModule { }
