import {
    Pipe,
    PipeTransform
} from '@angular/core';

// Por ser uma lib externa e que não faz parte do escopo @angular, para ser requerido como dependência em package.json, deve constar em "allowedNonPeerDependencies" no arquivo ng-package.json
import { PowerRange } from '@cyberjs.on/power-range';


@Pipe({
    name: 'getPagePer'
})
export class GetPagePerPipe implements PipeTransform {

    private pagination!: PowerRange.Pagination<Object>;

    constructor() { }

    transform<T extends Object>(collection: T[], limit?: number, pageNumber: number = 1): T[] {

        if (!collection || !collection.length) {
            return [];
        }

        if (this.pagination) {
            this.pagination.setLimit((limit)!);
        } else {
            this.pagination = new PowerRange.Pagination(collection, (limit)!);
        }

        return this.pagination.getPage(pageNumber) as T[];

    }

}
