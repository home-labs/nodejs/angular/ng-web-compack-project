import {
    Component,
    OnInit,
    OnDestroy,
    Input,
    Output,
    EventEmitter,
    ElementRef,
    ViewChild,
    HostListener
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import { Subscription } from 'rxjs';

import { LabelTranslationsProperties } from '../labelTranstationsProperties';


@Component({
    selector: 'lib-page-browser',
    templateUrl: './template.html',
    styleUrls: ['./style.less']
})
export class PageBrowserComponent

    implements
        OnInit,
        OnDestroy {

    @Input() labelTranslations!: LabelTranslationsProperties;

    @Input() enablePageNumberInputBox!: boolean;

    @Input() widthGrowthToggleFactor!: number;

    @Input() queryParamPropertyName!: string;

    @Output() changePage: EventEmitter<number>;

    set totalPages(value: number) {

        this._totalPages = value;

        this.maxlength = `${value}`.length;

        this.recordedPageNumber = this.currentPageNumber;
    }

    get totalPages(): number {
        return this._totalPages;
    }

    currentPageNumber!: number;

    bondedPageNumber!: number;

    firstPageLabel!: string | HTMLElement;

    previousPageLabel!: string | HTMLElement;

    nextPageLabel!: string | HTMLElement;

    lastPageLabel!: string | HTMLElement;

    recordedPageNumber: number;

    maxlength: number | null;

    showCurrentPageNumberDisplay: boolean;

    pageNumberInputBoxIsHidden: boolean;

    updatedAfterPageNumberInputBoxChanged: boolean;

    @ViewChild('pageNumberInputBox')
    protected set pageNumberInputBoxElementRef(value: ElementRef<HTMLInputElement>) {

        if (value) {
            this._pageNumberInputBox = value.nativeElement;

            this.resizePageNumberInputBoxWidth();
        }

    }

    @ViewChild('prevPageNavControl')
    protected set prevPageNavControlElementRef(value: ElementRef<HTMLAnchorElement>) {

        if (value) {
            this._prevPageNavControl = value.nativeElement;
        }

    }

    @ViewChild('nextPageNavControl')
    protected set nextPageNavControlElementRef(value: ElementRef<HTMLAnchorElement>) {

        if (value) {
            this._nextPageNavControl = value.nativeElement;
        }

    }

    @HostListener('document:click', ['$event'])
    onHostClick(event: PointerEvent) {

        const target = event.target;

        if (target !== this._prevPageNavControl && target !== this._nextPageNavControl) {
            this.bondedPageNumber = this.currentPageNumber;
        }

    }

    private _totalPages!: number;

    private queryParamsSubscription!: Subscription;

    private _pageNumberInputBox!: HTMLInputElement;

    private _prevPageNavControl!: HTMLAnchorElement;

    private _nextPageNavControl!: HTMLAnchorElement;

    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) {
        this.totalPages = 1;
        this.recordedPageNumber = 1;

        this.changePage = new EventEmitter();
        this.showCurrentPageNumberDisplay = true;
        this.pageNumberInputBoxIsHidden = true;

        this.initialize();
        this.maxlength = null;

        this.updatedAfterPageNumberInputBoxChanged = false;
    }

    ngOnInit() {

        if (this.queryParamPropertyName) {

            this.queryParamsSubscription = this.route.queryParams.subscribe(
                (params: any) => {

                    if (params.hasOwnProperty(this.queryParamPropertyName)) {
                        this.currentPageNumber = parseInt(params[this.queryParamPropertyName]);
                        this.navigate2(this.currentPageNumber);
                    }

                }
            );

        }

        this.resolveLabelTranslations();
    }

    ngOnDestroy() {

        if (this.queryParamsSubscription
            && this.queryParamsSubscription instanceof Subscription) {
            this.queryParamsSubscription.unsubscribe();
        }

    }

    resolveRightNavOfPageControlsShowing(): boolean {
        return this.bondedPageNumber >= this.currentPageNumber;
    }

    resolveLeftNavOfPageControlsShowing(): boolean {
        return this.bondedPageNumber <= this.currentPageNumber;
    }

    isOnFrontPage(): boolean {
        return this.currentPageNumber === 1;
    }

    isOnLastPage(): boolean {
        return this.currentPageNumber === this.totalPages;
    }

    getPreviousPage(): number {

        if (this.enablePageNumberInputBox) {

            if (this.bondedPageNumber !== this.currentPageNumber) {
                return this.bondedPageNumber;
            }

        }

        return this.currentPageNumber - 1;
    }

    getNextPage(): number {

        if (this.enablePageNumberInputBox) {

            if (this.bondedPageNumber !== this.currentPageNumber) {
                return this.bondedPageNumber;
            }

        }

        return this.currentPageNumber + 1;
    }

    navigate2(pageNumber: number) {

        this.currentPageNumber = pageNumber;

        this.changePage.emit(pageNumber);

        if (this.queryParamPropertyName) {
            this.router.navigate([location.pathname],
                {
                    queryParams: this.getQueryParamsAsLiteralObject(pageNumber),
                    queryParamsHandling: 'merge'
                }
            );
        }

        if (this.enablePageNumberInputBox) {
            this.bondedPageNumber = parseInt(`${pageNumber}`);


            this.updatedAfterPageNumberInputBoxChanged = true;


            if (this._pageNumberInputBox) {
                this._pageNumberInputBox.value = `${pageNumber}`;
                this.resizePageNumberInputBoxWidth();
            }

        }

    }

    enablesCurrentPageNumberDisplay() {

        if (this.enablePageNumberInputBox) {
            this.showCurrentPageNumberDisplay = true;
            this.pageNumberInputBoxIsHidden = true;
        }

    }

    enablesPageNumberInputBox() {

        if (this.enablePageNumberInputBox) {
            this.showCurrentPageNumberDisplay = false;
            this.pageNumberInputBoxIsHidden = false;
            this.resizePageNumberInputBoxWidth();
        }

    }

    focusOnPageNumberInputBox() {
        this._pageNumberInputBox.focus();
    }

    resolveInputBoxKeypress(event: KeyboardEvent) {
        // com keyup não funciona, e com o keydown bloqueia até o input
        event.preventDefault();
        this.resolveInputPageNumber(event.target as HTMLInputElement, event.key);
    }

    resizePageNumberInputBoxWidth() {

        const computedStyle = window.getComputedStyle(this._pageNumberInputBox);

        const pageNumberInputBoxLength: number = this._pageNumberInputBox.value.length;

        const internalWidthGrowthToggleFactor: number = pageNumberInputBoxLength * 0.5;

        const minimalWidth: number = Number.parseFloat(computedStyle.borderLeftWidth) +
            Number.parseFloat(computedStyle.paddingLeft) +
            Number.parseFloat(computedStyle.borderRightWidth) +
            Number.parseFloat(computedStyle.paddingRight);

        this._pageNumberInputBox.style.width = `${minimalWidth + (pageNumberInputBoxLength * this.widthGrowthToggleFactor) + internalWidthGrowthToggleFactor}px`;
    }

    private initialize() {
        this.currentPageNumber = 1;
        this.bondedPageNumber = this.currentPageNumber;
    }

    private getQueryParamsAsLiteralObject(pageNumber: number): object {

        const statement: any = {};

        statement[this.queryParamPropertyName] = pageNumber;

        return statement;
    }

    private resolveLabelTranslations() {

        if (Object.keys(this.labelTranslations).length) {
            if (this.labelTranslations.hasOwnProperty('firstPage')) {
                this.firstPageLabel = (this.labelTranslations.firstPage)!;
            } else {
                this.firstPageLabel = 'first';
            }

            if (this.labelTranslations.hasOwnProperty('previousPage')) {
                this.previousPageLabel = (this
                    .labelTranslations.previousPage)!;
            } else {
                this.previousPageLabel = 'previous';
            }

            if (this.labelTranslations.hasOwnProperty('nextPage')) {
                this.nextPageLabel = (this.labelTranslations.nextPage)!;
            } else {
                this.nextPageLabel = 'next';
            }

            if (this.labelTranslations.hasOwnProperty('lastPage')) {
                this.lastPageLabel = (this.labelTranslations.lastPage)!;
            } else {
                this.lastPageLabel = 'last';
            }
        } else {
            this.firstPageLabel = 'first';
            this.previousPageLabel = 'previous';
            this.nextPageLabel = 'next';
            this.lastPageLabel = 'last';
        }

    }

    private resolveInputPageNumber(target: HTMLInputElement, inputValue: string) {

        const selectionStart: number | null = target.selectionStart;

        const selectionEnd: number | null = target.selectionEnd;

        const targetValue: string = target.value;

        const numberPattern: RegExp = new RegExp(/^[0-9]+$/);

        let slice: string = ``;

        let sliceLeft: string = ``;

        let sliceRight: string = ``;

        let composedNumber: number = 0;

        if (numberPattern.test(inputValue)) {

            slice = targetValue.slice(0, targetValue.length);

            if (selectionStart === 0) {
                composedNumber = parseInt(`${inputValue}${slice}`);
            } else if (selectionStart as number < targetValue.length) {
                sliceLeft = targetValue.slice(0, selectionStart as number);
                sliceRight = targetValue
                    .slice(selectionEnd as number, targetValue.length);
                composedNumber = parseInt(`${sliceLeft}${inputValue}${sliceRight}`);
            } else {
                composedNumber = parseInt(`${slice}${inputValue}`);
            }

            if (composedNumber > 0 && composedNumber <= this.totalPages) {
                target.value = `${composedNumber}`;

                target.selectionStart = selectionStart as number + 1;
                target.selectionEnd = target.selectionStart;

                this.resizePageNumberInputBoxWidth();

                this.bondedPageNumber = composedNumber;


                this.updatedAfterPageNumberInputBoxChanged = false;
            }
        }
    }

}
