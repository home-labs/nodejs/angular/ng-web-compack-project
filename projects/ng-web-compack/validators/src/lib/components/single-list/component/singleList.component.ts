import {
    Component,
    Input
} from '@angular/core';
import {
    AbstractControl,
    FormGroup,
    NgForm
} from '@angular/forms';

import { FormControlsManager } from '../../../providers/index';


@Component({
    selector: 'lib-single-list-form-validation',
    templateUrl: './template.html',
    styleUrls: ['./style.less']
})
export class SingleListComponent {

    @Input()
    messages!: object;

    @Input()
    nameTranslations!: object;

    displayed: boolean;

    errorMessages: Array<string> = [];

    constructor(
        private formControlsManager: FormControlsManager
    ) {
        this.displayed = false;
    }

    validate(form: NgForm | FormGroup) {

        let mappedErrorKey!: string | null;

        let controls: object;

        let control: AbstractControl;

        if (form) {
            controls = form.controls;
            this.errorMessages = [];
            for (const fieldName of Object.keys(form.controls)) {
                control = (controls as any)[fieldName];
                mappedErrorKey = this.formControlsManager.getNextError4(
                    control,
                    Object.keys(this.messages)
                );

                if ((this.nameTranslations as any)[fieldName] && mappedErrorKey) {
                    this.errorMessages.push(`\n${(this.nameTranslations as any)[fieldName]} ${(this.messages as any)[mappedErrorKey]}`);
                }
            }

            if (mappedErrorKey) {
                this.displayed = true;
            }
        }
    }

}
