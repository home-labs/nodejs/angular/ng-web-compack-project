import {
    NgModule,
    ModuleWithProviders
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    FormsModule,
    ReactiveFormsModule
} from '@angular/forms';

import { Notifier } from '../../providers/index';

import { SingleListComponent } from './component/singleList.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        SingleListComponent
    ],
    exports: [
        FormsModule,
        ReactiveFormsModule,
        SingleListComponent
    ]
})
export class ListModule {

    static forChild(): ModuleWithProviders<ListModule> {
        return {
            ngModule: ListModule,
            providers: [
                Notifier
            ]
        };
    }

}
