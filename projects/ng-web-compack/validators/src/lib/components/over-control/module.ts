import {
    NgModule,
    ModuleWithProviders
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    FormsModule,
    ReactiveFormsModule
} from '@angular/forms';

import {
    Notifier,
    FormControlsManager
} from '../../providers/index';

import { OverControlComponent } from './component/overControl.component';


@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        OverControlComponent
    ],
    exports: [
        FormsModule,
        ReactiveFormsModule,
        OverControlComponent
    ]
})
export class OverControlModule {

    static forChild(): ModuleWithProviders<OverControlModule> {
        return {
            ngModule: OverControlModule,
            providers: [
                FormControlsManager,
                Notifier
            ]
        };
    }

}
