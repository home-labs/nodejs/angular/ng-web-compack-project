import {
  Component,
  Input,
  OnInit,
  OnDestroy
} from '@angular/core';

import { Subscription } from 'rxjs';

import {
  FormControlsManager,
  Notifier
} from '../../../providers';


@Component({
  selector: 'lib-over-control-form-validation',
  templateUrl: './template.html',
  styleUrls: ['./style.less']
})
export class OverControlComponent implements OnInit,
  OnDestroy {

  @Input()
  messages!: object;

  @Input()
  ngFormControl!: any;

  displayed: boolean;

  message!: string;

  errorMessages: Array<string> = [];

  private validationSubscription!: Subscription;

  constructor(
    private formControlsManager: FormControlsManager,
    private notifier: Notifier
  ) {
    this.displayed = false;
  }

  ngOnInit() {

    let mappedErrorKey: string | null;

    this.validationSubscription = this.notifier.subscribe(
      () => {
        mappedErrorKey = this.formControlsManager.getNextError4(
          this.ngFormControl,
          Object.keys(this.messages)
        );
        if (mappedErrorKey) {
          this.displayed = true;
          this.message = (this.messages as any)[mappedErrorKey];
        } else {
          this.message = '';
          this.displayed = false;
        }
      }
    );

  }

  ngOnDestroy() {
    this.validationSubscription.unsubscribe();
  }

}
