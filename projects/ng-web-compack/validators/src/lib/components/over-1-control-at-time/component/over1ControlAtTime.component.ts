import {
    Component,
    Input,
    OnInit,
    OnDestroy,
} from '@angular/core';
import { AbstractControl } from '@angular/forms';

import { Subscription } from 'rxjs';

import {
    FormControlsManager,
    Notifier
} from '../../../providers/index';


@Component({
    selector: 'lib-over-1-control-at-time-form-validation',
    templateUrl: './template.html',
    styleUrls: ['./style.less']
})
export class Over1ControlAtTimeComponent implements OnInit, OnDestroy {

    @Input()
    messages!: object;

    @Input()
    ngFormControl!: AbstractControl;

    @Input()
    nameTranslations!: object;

    displayed: boolean;

    message!: string;

    errorMessages: Array<string> = [];

    private validationSubscription!: Subscription;

    constructor(
        private formControlsManager: FormControlsManager,
        private notifier: Notifier
    ) {
        this.displayed = false;
    }

    ngOnInit() {

        let mappedErrorKey: string | null;

        this.validationSubscription = this.notifier.subscribe(
            () => {
                mappedErrorKey = this.formControlsManager.getNextError4(
                    this.ngFormControl,
                    Object.keys(this.messages)
                );

                if (mappedErrorKey) {

                    this.formControlsManager.add2Queue(this.ngFormControl);

                    if (this.formControlsManager.isNextOnQueue(this.ngFormControl)) {
                        this.displayed = true;
                        this.message = `${(this.messages as any)[mappedErrorKey]}`;
                    }
                } else {
                    this.formControlsManager.removeFromQueue(this.ngFormControl);
                    this.message = '';
                    this.displayed = false;
                }
            }
        );

    }

    ngOnDestroy(): void {
        this.validationSubscription.unsubscribe();
    }

}
