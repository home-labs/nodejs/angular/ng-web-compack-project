import {
    NgModule,
    ModuleWithProviders
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    FormsModule,
    ReactiveFormsModule
} from '@angular/forms';

import {
    FormControlsManager,
    Notifier
} from '../../providers/index';

import { Over1ControlAtTimeComponent } from './component/over1ControlAtTime.component';


@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        Over1ControlAtTimeComponent
    ],
    exports: [
        FormsModule,
        ReactiveFormsModule,
        Over1ControlAtTimeComponent
    ]
})
export class Over1ControlAtTimeModule {

    static forChild(): ModuleWithProviders<Over1ControlAtTimeModule> {
        return {
            ngModule: Over1ControlAtTimeModule,
            providers: [
                FormControlsManager,
                Notifier
            ]
        };
    }

}
