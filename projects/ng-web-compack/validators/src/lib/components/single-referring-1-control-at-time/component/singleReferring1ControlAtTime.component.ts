import {
    Component,
    Input,
    OnInit
} from '@angular/core';
import {
    AbstractControl,
    FormGroup,
    NgForm
} from '@angular/forms';

import { FormControlsManager } from '../../../providers/index';


@Component({
    selector: 'lib-single-referring-1-control-at-time-form-validation',
    templateUrl: './template.html',
    styleUrls: ['./style.less']
})
export class SingleReferring1ControlAtTimeComponent implements OnInit {

    @Input()
    messages!: object;

    @Input()
    nameTranslations!: object;

    displayed: boolean;

    message!: string;

    errorMessages: Array<string> = [];

    constructor(
        private formControlsManager: FormControlsManager
    ) {
        this.displayed = false;
    }

    ngOnInit() {

    }

    validate(form: NgForm | FormGroup) {

        let mappedErrorKey: string | null;

        let controls: object;

        let control: AbstractControl;

        if (form) {
            controls = form.controls;
            for (const k of Object.keys(controls)) {
                control = (controls as any)[k];
                mappedErrorKey = this.formControlsManager.getNextError4(
                    control,
                    Object.keys(this.messages)
                );
                if (mappedErrorKey) {
                    this.displayed = true;
                    this.message = `${(this.nameTranslations as any)[k]} ${(this.messages as any)[mappedErrorKey]}`;
                    break;
                } else {
                    this.message = '';
                    this.displayed = false;
                }
            }
        }
    }

}
