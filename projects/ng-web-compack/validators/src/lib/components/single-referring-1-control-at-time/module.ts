import {
    NgModule,
    ModuleWithProviders
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    FormsModule,
    ReactiveFormsModule
} from '@angular/forms';

import { Notifier } from '../../providers/index';

import { SingleReferring1ControlAtTimeComponent } from './component/singleReferring1ControlAtTime.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        SingleReferring1ControlAtTimeComponent
    ],
    exports: [
        FormsModule,
        ReactiveFormsModule,
        SingleReferring1ControlAtTimeComponent
    ]
})
export class SingleReferring1ControlAtTimeModule {

    static forChild(): ModuleWithProviders<SingleReferring1ControlAtTimeModule> {
        return {
            ngModule: SingleReferring1ControlAtTimeModule,
            providers: [
                Notifier
            ]
        };
    }

}
