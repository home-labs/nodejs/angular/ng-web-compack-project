import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';


@Injectable()
export class FormControlsManager {

    private formControlsQueue: AbstractControl[];

    constructor() {
        this.formControlsQueue = [];
    }

    isNextOnQueue(formControl: AbstractControl): boolean {
        return this.formControlsQueue[0] === formControl;
    }

    add2Queue(formControl: AbstractControl) {

        if (!this.formControlsQueue.includes(formControl)) {
            this.formControlsQueue.push(formControl);
        }

    }

    removeFromQueue(formControl: AbstractControl) {

        const index = this.formControlsQueue.findIndex(
            (item: AbstractControl): boolean => {
                if (item === formControl) {
                    return true;
                }

                return false;
            }
        );

        this.formControlsQueue.splice(index, 1);
    }

    // https://angular.io/guide/form-validation
    getNextError4(control: AbstractControl, validationTypes: Array<string>): string | null {

        let error: string;

        let errorKeys: Array<string> = [];

        if (control.invalid) {
            errorKeys = Object.keys(control.errors as any);
            for (error of errorKeys) {
                if (validationTypes.indexOf(error) !== -1) {
                    return error;
                }
            }
        }

        return null;
    }

    // getControlName(formControl: AbstractControl): string {

    //     const parent = formControl.parent;

    //     let controlName = ``;

    //     if (parent instanceof FormGroup) {
    //         for (let currentControlName of Object.keys(parent.controls)) {
    //             if (parent.controls[currentControlName] === formControl) {
    //                 controlName = currentControlName;
    //                 break;
    //             }
    //         }
    //     }

    //     return controlName;

    // }

    // resetForm(ngForm: NgForm) {
    //     const
    //         valuesCopy: object = this.copyValues(ngForm);

    //     ngForm.resetForm(valuesCopy);
    // }

    // private copyValues(ngForm: NgForm): object {
    //     const
    //         valuesMap: object = {};

    //     Object.keys(ngForm.controls).forEach(control => {
    //         (valuesMap as any)[control] = ngForm.controls[control].value;
    //     });

    //     return valuesMap;
    // }

}
