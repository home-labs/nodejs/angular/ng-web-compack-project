import { Injectable } from '@angular/core';

import {
    Subject,
    Subscription
} from 'rxjs';


@Injectable()
export class Notifier {

    private validationSubscription: Subject<void> = new Subject();

    // constructor(control: AbstractControl, validationTypes: Array<string>) {
    constructor() {

    }

    // the control would can be passed by here, but for some reason the control still doesn’t exist in any default initialization event
    subscribe(callback: () => void): Subscription {
        return this.validationSubscription.subscribe(callback as any);
    }

    notify() {
        this.validationSubscription.next();
    }

}
