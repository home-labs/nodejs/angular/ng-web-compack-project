# Validators

## Installing

	$ npm i @cyberjs.on/ng-web-compack --save

## Usage

Choose which component you want to use:

- lib-list-form-validation;
- lib-over-control-form-validation;
- lib-over-control-at-time-form-validation>;
- lib-single-4-1-control-at-time-form-validation.

### OverControlModule

Include the module into `imports` metadata key of `NgModule` decorator of your application importing `OverControlModule` from `@cyberjs.on/ng-web-compack/validators`, like that.

```ts
import { OverControlModule } from '@cyberjs.on/ng-web-compack/validators';

@NgModule({
    imports: [
        OverControlModule.forChild()
    ]
})
export class MyModule() { }
```

Include `Validators` namespace from  `@cyberjs.on/ng-web-compack/validators`in your component and inject `Validators.Notifier` as dependency.

```ts
@Component({
    selector: 'app-form-validation',
    templateUrl: './template.html',
    styleUrls: ['./style.less']
})
export class OverControlExampleComponent implements OnInit {

    form: FormGroup;

    constructor(
        private validator: Validators.Notifier,
        private formBuilder: FormBuilder
    ) {

        this.form = this.formBuilder.group({
            name: new FormControl('',
                {
                    validators: [AngularValidators.required, AngularValidators.minLength(4)]
                    , updateOn: 'blur'
                }
            )
            , name2: new FormControl('',
                {
                    validators: [AngularValidators.required]
                    , updateOn: 'blur'
                }
            )
        });

    }

    ngOnInit() {

    }

    onSubmit() {
        this.validator.notify();
    }

}
```

To use anyone you must define the `messages` property binding.

```html
<lib-over-control-form-validation
	[messages]="{
		required: 'this field is required'
	}"
></lib-over-control-form-validation<form
  [formGroup]="form"
  (ngSubmit)="onSubmit()"
  >

  <div class="line">
    <input
      formControlName="name"
      placeholder="nome"
    />

    <lib-over-control-form-validation
      [ngFormControl]="form.controls['name']"
      [messages]="{
        required: 'cannot be blank',
        minlength: 'must be at least 4 characters'
      }"
    ></lib-over-control-form-validation>
  </div>

  <div class="line">
    <input
      formControlName="name2"
      placeholder="nome 2"
    />

    <lib-over-control-form-validation
      [ngFormControl]="form.controls['name2']"
      [messages]="{
        required: 'Não pode ficar em branco',
        minlength: 'Deve ter no mínimo 4 caracteres'
      }"
    ></lib-over-control-form-validation>
  </div>


  <div class="actions">
    <button type="submit">Salvar</button>
  </div>

</form>
>
```

### Single41ControlAtTimeModule

Include the module `Single41ControlAtTimeModule` from `@cyberjs.on/ng-web-compack/validators` in your module.

Include `Validators` namespace from  `@cyberjs.on/ng-web-compack/validators`in your component.

To use `lib-single-4-1-control-at-time-form-validation` and `lib-list-form-validation` you must to add the `nameTranslations` property binding and define a variable to bind a property by `@ViewChild('')`.

```html
<lib-single-4-1-control-at-time-form-validation
	#single41ControlAtTimeValidator
	[nameTranslations]="{
		name: 'Name',
		name2: 'Name 2'
	}"
></lib-single-4-1-control-at-time-form-validation>
```

And in the component...

```ts
@ViewChild('single41ControlAtTimeValidator', { static: false })
    private single41ControlAtTimeValidator!: Validators.Single41ControlAtTimeComponent;
```

And call the `this.single41ControlAtTimeValidator.validate(form: FormGroup | NgForm)` method.

<!--stackedit_data:
eyJoaXN0b3J5IjpbNzMyNzg0NjcsNTc2MjE2NjY1LDE2NTk1Mz
A3NjIsLTExMjE1NTAzMTIsMTUzODY5MjYwNCwxNzc2MDQ1NTgz
LDQzODM5NTI2OSwtNDEwMTY1MDgyLC0yMDI2ODEyMzg3LC0yMD
E3MDE5MTI5LDEwODc5NTIwOTMsLTIzOTA1MDg4MiwxODIzMTA3
NDkzLC0yMDkzNzEwODFdfQ==
-->
