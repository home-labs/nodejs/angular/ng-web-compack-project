import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SidePanelComponent } from './component/sidePanel.component';


@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        SidePanelComponent
    ],
    exports: [
        CommonModule,
        SidePanelComponent
    ]
})
export class SidePanelModule { }
