import {
    Component,
    ViewChild,
    HostListener,
    ElementRef,
    Input,
    AfterViewInit
} from '@angular/core';


@Component({
    selector: 'lib-side-panel',
    templateUrl: './template.html',
    styleUrls: ['./style.less']
})
export class SidePanelComponent implements AfterViewInit {

    @Input()
    private retracted!: boolean;

    private computedWidth!: string;

    private inlineStyle!: CSSStyleDeclaration;

    private _container!: HTMLDivElement;

    private triggerElements: EventTarget[];

    private isAReleaseTriggerElement: boolean;

    @ViewChild('container')
    private set containerRef(value: ElementRef<HTMLDivElement>) {

        if (value) {
            this._container = value.nativeElement;

            this.inlineStyle = this._container.style;
        }

    }

    @HostListener('document:click', ['$event'])
    onHostClick(event: PointerEvent) {

        if (!this.retracted) {

            if (this.isAReleaseTriggerElement) {
                this.recordTriggerElement(event);
            }

            if (!this.triggerElements.includes(event.target!)
                && !this._container.contains(event.target as Node)
            ) {
                this.recall();
            }

        }

    }

    constructor() {
        // this.retracted = this.retracted || false;
        this.retracted = this.retracted || true;
        this.triggerElements = [];
        this.isAReleaseTriggerElement = false;
    }

    ngAfterViewInit() {

        this.setComputedWith();

        if (this.retracted) {
            this.inlineStyle.width = '0px';
        } else {
            this.inlineStyle.width = this.computedWidth;
        }

    }

    toggle() {
        if (this.retracted) {
            this.isAReleaseTriggerElement = true;
            this.setComputedWith();
            this.release();
        } else {
            this.recall();
        }
    }

    private setComputedWith() {

        let containerParent: Node | null;

        let containerClone: HTMLElement;

        let computedStyle: CSSStyleDeclaration;

        let inlineStyle: CSSStyleDeclaration;

        containerClone = this._container.cloneNode(true) as HTMLElement;

        inlineStyle = containerClone.style;
        inlineStyle.visibility = 'hidden';

        containerParent = this._container.parentElement;
        computedStyle = window.getComputedStyle(containerClone);

        if (containerParent) {
            containerParent.appendChild(containerClone);
            inlineStyle.width = '';

            this.computedWidth = computedStyle.width;

            containerParent.removeChild(containerClone);
        } else {
            this.computedWidth = `${window.document.documentElement.offsetWidth}px`;
        }

    }

    private release() {
        this.inlineStyle.width = this.computedWidth;
        this.retracted = false;
    }

    private recall() {
        this.inlineStyle.width = '0px';
        this.retracted = true;
    }

    private recordTriggerElement(event: Event) {

        const eventTarget: EventTarget = event.target!;

        if (eventTarget && !this.triggerElements.includes(eventTarget)) {
            this.triggerElements.push(eventTarget);
        }

        this.isAReleaseTriggerElement = false;
    }

}
