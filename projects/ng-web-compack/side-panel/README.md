# SidePanel

## Installing

	$ npm i @cyberjs.on/ng-web-compack --save

## API reference

### Usage

Include the module into `imports` metadata key of `NgModule` decorator in your application, importing `SidePanelModule` from `@cyberjs.on/ng-web-compack/side-panel`, like that.

```typescript
import { SidePanelModule  } from '@cyberjs.on/ng-web-compack/side-panel';

@NgModule({
    imports: [
        SidePanelModule
    ]
})
export class MyModule() { }
```

### Directives

Selector: `lib-side-panel`
Exported as:  `SidePanelComponent`

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTIzNjEyMzA0OSw5Mzc5NDQ2MzRdfQ==
-->